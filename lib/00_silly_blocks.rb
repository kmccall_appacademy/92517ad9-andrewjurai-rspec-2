#reverser: reverses the order of the letters in a string

def reverser(&block)
  string = block.call
  word_array = string.split
  word_array.map(&:reverse).join(" ")
end

#adder: takes a value from a block and adds the value of the argument which if not specified, is 1

def adder(num1 = 1, &num2)
  num1.add num2.call
end

class Numeric
  def add(num2)
    [self, num2].reduce(:+)
  end
end

#repeater: executes the passed block the number of times specified by the argument, the default argument is

def repeater(amt = 1, &block)
  amt.times { block.call }
end
