# ### Factors
#
# Write a method `factors(num)` that returns an array containing all the
# factors of a given number.

def factors(num)
  (1..num).select { |int| int if int.is_a_factor_of? num }
end

class Numeric

  def is_a_factor_of?(int)
    int % self == 0
  end

end


# ### Bubble Sort
#
# http://en.wikipedia.org/wiki/bubble_sort
#
# Implement Bubble sort in a method, `Array#bubble_sort!`. Your method should
# modify the array so that it is in sorted order.
#
# > Bubble sort, sometimes incorrectly referred to as sinking sort, is a
# > simple sorting algorithm that works by repeatedly stepping through
# > the list to be sorted, comparing each pair of adjacent items and
# > swapping them if they are in the wrong order. The pass through the
# > list is repeated until no swaps are needed, which indicates that the
# > list is sorted. The algorithm gets its name from the way smaller
# > elements "bubble" to the top of the list. Because it only uses
# > comparisons to operate on elements, it is a comparison
# > sort. Although the algorithm is simple, most other algorithms are
# > more efficient for sorting large lists.
#
# Hint: Ruby has parallel assignment for easily swapping values:
# http://rubyquicktips.com/post/384502538/easily-swap-two-variables-values
#
# After writing `bubble_sort!`, write a `bubble_sort` that does the same
# but doesn't modify the original. Do this in two lines using `dup`.
#
# Finally, modify your `Array#bubble_sort!` method so that, instead of
# using `>` and `<` to compare elements, it takes a block to perform the
# comparison:
#
# ```ruby
# [1, 3, 5].bubble_sort! { |num1, num2| num1 <=> num2 } #sort ascending
# [1, 3, 5].bubble_sort! { |num1, num2| num2 <=> num1 } #sort descending
# ```
#
# #### `#<=>` (the **spaceship** method) compares objects. `x.<=>(y)` returns
# `-1` if `x` is less than `y`. If `x` and `y` are equal, it returns `0`. If
# greater, `1`. For future reference, you can define `<=>` on your own classes.
#
# http://stackoverflow.com/questions/827649/what-is-the-ruby-spaceship-operator
#bubble_sort: sorts in ascending oder
class Array
  attr_accessor :block

  def bubble_sort!(&block)
    return self if self.length <= 1
    @block = block if block_given?
    until self.sort_complete?
      sort_ascendingly
    end
    return self.reverse unless @block.nil?
    self
  end

  def sort_ascendingly
    self.each_with_index do |num1, i|

      last_indx = self.length - 1
      break if i == last_indx
      num2 = self[i + 1]

      # if @block.nil? #block  passed
      #   self.swap_values!(i) if wrong_sequence?(num1, num2,)
      self.swap_values!(i) if wrong_sequence?(num1, num2, @block)

    end
     self
  end #sort the array in ascending order

  def sort_complete?
    i = 0
    until i == self.length - 1
      set = self[i..i + 1]
      return false if wrong_sequence?(*set, @block)
      i += 1
    end
    true
  end #checks to see whether the array is sorted ascendingly

  def wrong_sequence?(num1, num2, block)
    if block_given?
      block.call(num1, num2) == 1
    else num1 > num2
    end
  end  #checks to see if the proceeding value is higher thant the predessor

  def swap_values!(index)

    self[index], self[index + 1] = self[index + 1], self[index]
  end  #swaps the current value with that in front it

  def bubble_sort(&prc)
    self.dup.bubble_sort!(&prc)
  end

end



# ### Substrings and Subwords
#
# Write a method, `substrings`, that will take a `String` and return an
# array containing each of its substrings. Don't repeat substrings.
# Example output: `substrings("cat") => ["c", "ca", "cat", "a", "at",
# "t"]`.
#
# Your `substrings` method returns many strings that are not true English
# words. Let's write a new method, `subwords`, which will call
# `substrings`, filtering it to return only valid words. To do this,
# `subwords` will accept both a string and a dictionary (an array of
# words).

def substrings(string)
  str_chars = string.chars
  substrings_array = []
  str_chars.each_index do |current_indx|
    substrings_array += str_chars.create_substrings_from(current_indx)
  end
  substrings_array.uniq
end


class Array

  def create_substrings_from(current_indx)
    working_array = self[current_indx..-1]
    working_array.map.with_index do |_, incrementer_indx|
      working_array[0..incrementer_indx].join
    end
  end

end

def subwords(word, dictionary)
  substrings = substrings(word)
  substrings.select { |substr| word if dictionary.include? substr }
end


# ### Doubler
# Write a `doubler` method that takes an array of integers and returns an
# array with the original elements multiplied by two.

def doubler(array)
  array.map(&:x2)
end

class Numeric
  def x2
    self * 2
  end
end


# ### My Each
# Extend the Array class to include a method named `my_each` that takes a
# block, calls the block on every element of the array, and then returns
# the original array. Do not use Enumerable's `each` method. I want to be
# able to write:
#
# ```ruby
# # calls my_each twice on the array, printing all the numbers twice.
# return_value = [1, 2, 3].my_each do |num|
#   puts num
# end.my_each do |num|
#   puts num
# end
# # => 1
#      2
#      3
#      1
#      2
#      3
#
# p return_value # => [1, 2, 3]
# ```

class Array
  def my_each(&prc)
    i = 0
    while i < self.length
      element = self.dup[i]
      prc.call(element)
      i += 1
    end
    self
  end
end

# ### My Enumerable Methods
# * Implement new `Array` methods `my_map` and `my_select`. Do
#   it by monkey-patching the `Array` class. Don't use any of the
#   original versions when writing these. Use your `my_each` method to
#   define the others. Remember that `each`/`map`/`select` do not modify
#   the original array.
# * Implement a `my_inject` method. Your version shouldn't take an
#   optional starting argument; just use the first element. Ruby's
#   `inject` is fancy (you can write `[1, 2, 3].inject(:+)` to shorten
#   up `[1, 2, 3].inject { |sum, num| sum + num }`), but do the block
#   (and not the symbol) version. Again, use your `my_each` to define
#   `my_inject`. Again, do not modify the original array.

class Array

  def my_map(&prc)
    i = 0
    return_arr = Array.new
    while i < self.length
      element = self.dup[i]
      return_arr << prc.call(element)
      i += 1
    end
    return_arr
  end

  def my_select(&prc)
    i = 0
    return_arr = Array.new
    while i < self.length
      element = self.dup[i]
      return_arr << element if prc.call(element)
      i += 1
    end
    return_arr
  end

  def my_inject(&blk)

    acc = self.dup[0]
    i = 1
    while i < self.length
      element = self.dup[i]
      acc = blk.call(acc, element)
      i += 1
    end
    acc
  end

end

# ### Concatenate
# Create a method that takes in an `Array` of `String`s and uses `inject`
# to return the concatenation of the strings.
#
# ```ruby
# concatenate(["Yay ", "for ", "strings!"])
# # => "Yay for strings!"
# ```

def concatenate(strings)
  strings.inject { |acc, str| acc + str }
end
