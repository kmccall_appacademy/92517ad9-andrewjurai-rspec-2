#Performance Monitor: measure how long it takes for a block to execute

def measure(amt = 1, &block)
  run_times = []
  amt.times do
    before = Time.now
    block.call
    after = Time.now
    total_elapsed = after - before
    run_times << total_elapsed
  end
  run_times.avg_time
end

class Array
  def avg_time
    sum = self.reduce(:+)
    sum / self.length
  end
end
